const express = require("express");
const path = require("path");
var axios = require("axios");
var qs = require("qs");
var cors = require("cors");
const fileUpload = require('express-fileupload');
require('custom-env').env('development');


console.log(__dirname);

const app = express(),
  bodyParser = require("body-parser");
port = process.env.PORT;
console.log(process.env.PORT);

// middle ware
app.use(express.static('public')); //to access the files in public folder
app.use(cors()); // It enables all cors request
app.use(fileUpload());

const API_ENDPOINT = `https://${process.env.ZESTY_INSTANCE_ZUID}.api.zesty.io/v1/content/models/${process.env.ZESTY_INSTANCE_MODULE_ZUID}/items`; // Testing Legal Modal 
const users = [];

app.use(bodyParser.json());
app.use(
  express.static(path.join("../ftdr-terms-conditions-creation-new/build"))
);

app.get("/", (req, res) => {
  res.json({
    status: false,
    message: "Please enter valid route, route not found",
  });
});

app.post("/api/createitem/", (req, res) => {
	// const title = req.body.item.title;
	// const subtitle = req.body.item.subtitle;
	const content = req.body.item.content;
	console.log(req.body.item);

  	var data = JSON.stringify({
		web: {
		metaDescription: "",
		metaTitle: req.body.item.subtitle,
		metaLinkText: req.body.item.subtitle,
		metaKeywords: null,
		parentZUID: "0",
		path: "",
		sitemapPriority: -1,
		canonicalTagMode: 1,
		canonicalQueryParamWhitelist: null,
		},
		data: { 
			title: 		req.body.item.title, 
			subtitle: 	req.body.item.subtitle, 
			buttonlable: 		req.body.item.buttonLable, 
			successtitle: 		req.body.item.successTitle, 
			success_message: 		req.body.item.successMessage, 
			content: 	content 
		},
	});

	var zestyconfig = {};
	zestyconfig = {
		method: "post",
		url: API_ENDPOINT,
		headers: {
		Authorization: `Bearer ${process.env.ZESTY_INSTANCE_TOKEN}`, // Bearer Token
		"Content-Type": "application/json",
		},
		data: data,
	};
	console.log(zestyconfig);

	axios(zestyconfig)
		.then(function (response) {
			console.log(response);
			if( response.data ){
				const data = JSON.stringify(response.data);
				console.log(data);
				
				if(response.data.data)
					res.json({status: true, message: "item is created", data: data });
				else
					res.json({status: false, message: response.message, response: response  });
			}else{
				res.json({status: false, message: response.message, response: response  });
			}
		})
		.catch(function (error) {
			console.log(error);
			res.json({status: false, message: error.message, error: error});
		});
});

app.use(function (req, res) {
  res.status(404).send({ url: req.originalUrl + " not found", status: false });
});

app.listen(port, () => {
  console.log(`Server listening on the port::${port}`);
});
