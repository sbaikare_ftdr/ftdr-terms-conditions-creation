import React, { Component } from "react";
import "@ftdr/blueprint-core/assets/css/default-theme.css";
import MyHeader from "./components/Header/Header";
import LegalForm from "./components/LegalForm";
import axios from 'axios';
import { convertDocument, createItem } from "./services/ZestyService";
import {
  Dialog
} from "@ftdr/blueprint-components-react";

const initialState = {
  inputFile: null,
  dialogOpen: false,
  message: "",
  item: {
    title: "",
    subtitle: "",
    buttonLable: "",
    successTitle: "",
    successMessage: "",
    selectedFile: null,
    content: null,
    content_1: "",
    content_2: "",
    content_3: "",
  },
  errors:{
    errTitle: "",
    errSubtitle: "",
    errInputfile: "",
    file: ""
  }
  
  
};
// Max count in each wiswig editor is 65K around
// const MAX_CHAR_COUNT = 8000;
// const MAX_FILE_SIZE = 5; // Max File size 5MB
// const MAX_FILE_SIZE_BYTES = 1024 * 1024 * MAX_FILE_SIZE;

class App extends Component {
  
  state = initialState;

  // validate = () => {
  //   let item = this.state.item;
  //   let titleError = "";
  //   let subtitleError = "";
  //   let inputfileError = "";

  //   if ( item.title === '') {
  //     titleError = "Title is required!";
  //   }

  //   if (item.subtitle === '' ) {
  //     subtitleError = "Subtitle is required!";
  //   }
  //   console.log(item.selectedFile.size);
  //   var documents = [
  //                 "application/msword", 
  //                 "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
  //                 "application/vnd.oasis.opendocument.text"
  //               ];
  //   var validFileType = documents.includes(item.selectedFile.type);

  //   if (!validFileType) {      
  //     inputfileError += "Uploaded file type is not correct! ";
  //   }

  //   if (item.selectedFile.size >= MAX_FILE_SIZE_BYTES ) {
  //     inputfileError += "Uploaded file size should not be more than 5MB!";
  //   }

  //   //console.log(this.state.errors);
  //   if (titleError!= '' || subtitleError != '') {
  //     this.setState(prevState => ({
  //       errors: {                   // object that we want to update
  //           ...prevState.errors,    // keep all other key-value pairs
  //           errTitle: titleError,       // update the value of specific key
  //           errSubtitle: subtitleError,
  //           errInputfile: inputfileError
  //       }
  //     }))
  //     return false;
  //   }

  //   return true;
  // };

  createItem = async(e) => {
    //e.preventDefault();
    // const isValid = this.validate();
    // console.log('validation');
    // console.log(this.state);
    // if (isValid){
    //   this.setState(initialState);
    //   return false;
    // }

    const item = this.state.item;
    const formData = new FormData();

    formData.append("inputFile", this.state.item.selectedFile);

    await axios.post('https://api.docconversionapi.com/convert?outputFormat=HTML', formData, {
      headers: {
        "X-ApplicationID": `${process.env.REACT_APP_DOC_CONVERSION_APP_ID}`, //process.env.DOC_CONVERSION_APP_ID,
        "X-SecretKey": `${process.env.REACT_APP_DOC_CONVERSION_SECRETE_ID}` //process.env.DOC_CONVERSION_SECRETE_ID
      },
    })
      .then(function (response) {
        item.content = response.data;        
        createItem(item).then((response) => {
          console.log(response);
          if (response.data) {
            const data = JSON.parse(response.data);
            if (response.status == true) {
              console.log(data.data.ZUID);
              alert(
                `Zesty Item created, here is preview URL: ${process.env.REACT_APP_ZESTY_INSTANCE_API_URL}/legal-page.json?zuid=` +
                  data.data.ZUID
              );
              //this.state.dialogOpen = true;
            } else {
              alert(response.message);
            }
          } else {
            alert(response.message);
          }
        });
      })
      .catch(function (error) {
        console.log(error);
        //this.setState({ dialogOpen: true, message: error.message });
      });    
  }

  onChangeForm = (e) => {
    const { item } = this.state;
    if (e.target.name === "title") {
      item.title = e.target.value;
    } else if (e.target.name === "subtitle") {
      item.subtitle = e.target.value;
    } else if (e.target.name === "buttonlable") {
      item.buttonLable = e.target.value;
    } else if (e.target.name === "successtitle") {
      item.successTitle = e.target.value;
    } else if (e.target.name === "successmessage") {
      item.successMessage = e.target.value;
    } else if (e.target.name === "file") {
      item.selectedFile = e.target.files[0];
      //this.setState({ inputFile: e.target.files[0] });
      //item.content = this.convertDoc(e.target.files[0]);
    }
    //console.log(this.state);
    this.setState({ item });
  };

  render() {
    
    return (
      <div className="App">
        <MyHeader />
        <LegalForm
          data={this.state}
          onChangeForm={this.onChangeForm}
          createItem={this.createItem}          
        /> 

        {this.state.dialogOpen ? 
         <Dialog
            open={true}
            onClose={() => this.setState({ dialogOpen:false, message: null}) }
            header="Message Dialog"            
            modal={false}
            actions={2}
            >
            <div>{this.message}</div>
        </Dialog>    
        : null
        } 
      </div>
    );
  }
}

export default App;
