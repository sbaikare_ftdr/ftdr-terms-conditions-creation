export async function convertDocument(data) {
    const response = await fetch(`/api/convertdocument/`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({item: data})
      })
      console.log("in services");
      console.log(data);
    return await response.json();
}

export async function createItem(data) {
  const response = await fetch(`/api/createitem/`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({item: data})
    })
    console.log("in services");
    console.log(data);
  return await response.json();
}