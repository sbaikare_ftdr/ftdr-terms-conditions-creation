import { Logo } from "@ftdr/blueprint-components-react";
import React from "react";

const MyHeader = () => {
  // const {
  //   appSettings: { getCustom },
  // } = useAppContext();
  // const config: any = getCustom();
  // const logoUrl = config && config.authConfig ? config.authConfig.frontdoor_url : "";
  return (
    <div className="border-b-1 border-gray-400 flex justify-center pt-8 pb-8">
      <Logo
              svgProps={{ width: 250 }}
              brand="frontdoor"
              variant="logo"
              monochrome={false}
              className="pl-5"
            />
    </div>
  );
};

export default MyHeader;
