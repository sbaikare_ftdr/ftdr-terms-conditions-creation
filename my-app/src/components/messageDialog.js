import React, { useState, useEffect } from "react";
import {
    Dialog
  } from "@ftdr/blueprint-components-react";

const messageDialog = props => {
    return (

        <Dialog
            open={props.data.dialogOpen}
            onClose={() => props.data.dialogOpen = false }
            header="Message Dialog"            
            modal="false"
            >
            <div>This is test modal</div>
        </Dialog>
    )
}
export default messageDialog;