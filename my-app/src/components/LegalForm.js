import React from "react";
import {
    Button,
    //IconNavArrowRight,
    Input,
    //Link,
    Panel,
    //RadioGroup,
    Text,
    //useAppContext,
  } from "@ftdr/blueprint-components-react";
  
const Results = () => (
    <div id="results" className="search-results">
      Some Results
    </div>
)   

const LegalForm = ({data, onChangeForm, createItem }) =>{

  return (
        <Panel
            shadow={true}
            className="membership-panel w-1/2 p-6 mx-auto mt-10"
            borderColor="gray-300"
        >
            <div className="mb-8">
                <Text variant="heading-04">Document to HTML conversion</Text>
            </div>
            <div className="mb-8">
                <Input 
                label="Legal page Title *" 
                onChange={(e) => onChangeForm(e)}                
                type="text"  
                required="true"
                textLength={{ max: 20, current: data.item.title.length }}
                maxLength={20}
                formField
                name="title"
                hint="Please enter legal page title"
                />
                <div style={{ fontSize: 12, color: "red" }}>
                    {data.errors.errTitle}
                </div>
            </div>           
            
            <div className="mb-8">
                <Input 
                //required="true"                
                type="text"  
                name="subtitle"
                label="Legal page Subtitle *" 
                onChange={(e) => onChangeForm(e)}
                formField
                hint="Please enter legal page subtitle"
                />
                <div style={{ fontSize: 12, color: "red" }}>
                    {data.errors.errSubtitle}
                </div>
            </div>
            
            <div className="mb-8">
                <Input 
                label="Button Lable" 
                onChange={(e) => onChangeForm(e)}                
                type="text"  
                //required="true"
                formField
                name="buttonlable"
                hint="Please enter Button Lable"
                />
            </div>

            <div className="mb-8">
                <Input 
                label="Success Title" 
                onChange={(e) => onChangeForm(e)}                
                type="text"  
                //required="true"
                formField
                name="successtitle"
                hint="Please enter Success message title"
                />
            </div>

            <div className="mb-8">
                <Input 
                label="Success Message" 
                onChange={(e) => onChangeForm(e)}                
                type="text"  
                //required="true"
                formField
                name="successmessage"
                hint="Please enter Success Message"
                />
            </div>

            <div className="flex justify-between mt-8">
            <Input
                label="Upload document"                
                type="file"     
                name="file"
                onChange={(e) => onChangeForm(e)}                                
                formField
                hint="Please upload the word document."

                />  
                <span style={{ fontSize: 12, color: "red" }}>
                    {data.errors.errInputfile}
                </span>
        </div>
        <div className="flex justify-between mt-8">
                
            <Button
                onClick= {(e) => createItem()}
                label="Upload document"
                size="medium"
            />
        </div>


    </Panel>
  );
};

export default LegalForm;
